const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.addNewKeyValue = functions.database.ref("/com_list/{pushId}")
    .onCreate((snapshot)=>{
        let newValue = snapshot.val().comment + "QQ";
        return snapshot.ref.child('addedQQdata').set(newValue);
    });